#!/bin/bash

FILES=`find . -mindepth 1 -maxdepth 4 -type f -regex ".*_config*.yaml"`

#Binary check
if ! which sops > /dev/null; then
  >&2 echo "Error finding 'sops' on PATH!"
  exit 1;
fi

for i in $FILES; do
  NEW_FILENAME=$(echo -n "$i" | sed 's/yaml/enc.yaml/g')
  sops --encrypt $i > $NEW_FILENAME || exit 1;
  rm $i
done


## dont remove it
#FILES=`find . -type f -regex ".*./config/*.yaml"`

## My super regex!
#find test -type f -regex ".*-config*.yaml"

#FILES=`find ./ -type f -regex ".*-config*.yaml"`

#triger1
