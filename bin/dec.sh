#!/bin/bash

FILES=`find . -mindepth 1 -maxdepth 4 -type f -regex ".*_config*.*enc.*yaml"`

if ! which sops > /dev/null; then
  >&2 echo "Error finding 'sops' on PATH!"
  exit 1;
fi

for i in $FILES; do
  NEW_FILENAME=$(echo -n "$i" | sed 's/enc.yaml/yaml/g')
  sops --decrypt $i > $NEW_FILENAME
done

# FILES=`find ./ -type f -regex ".*-config*.*enc.*yaml"`